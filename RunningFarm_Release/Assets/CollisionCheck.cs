﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCheck : MonoBehaviour
{
    // Start is called before the first frame update
    private float MagneticSpeed = 12.0f;
    private bool IsMargnetic;
    private AudioSource EattingSound;
    private GameObject player;
    void Start()
    {
        IsMargnetic = false;
        EattingSound = GameObject.Find("Eatting").GetComponent<AudioSource>();
        player = GameObject.Find("Runner");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player.GetComponent<PlayerMovement>().IsMargnet && IsMargnetic)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, player.transform.position, Time.deltaTime * MagneticSpeed);
            transform.position = pos;
        }
    }

    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if(player.GetComponent<SphereCollider>() == other && player.GetComponent<PlayerMovement>().IsMargnet)
        {
            IsMargnetic = true;
        }
        if (player.GetComponent<CapsuleCollider>() == other)
        {
            player.GetComponent<PlayerMovement>().EatingNum++;
            EattingSound.Play();
            if (player.GetComponent<PlayerMovement>().IsDead)
                EattingSound.Stop();
            Destroy(this.gameObject);
        }


    }
}
