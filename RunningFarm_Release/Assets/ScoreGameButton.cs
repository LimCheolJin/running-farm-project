﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreGameButton : MonoBehaviour
{
    public AudioSource sound;
    public void Button()
    {
        sound.Play();
        Invoke("startScore", 0.1f);
    }

    private void startScore()
    {
        SceneManager.LoadScene("HighScoreScene");

    }
}
