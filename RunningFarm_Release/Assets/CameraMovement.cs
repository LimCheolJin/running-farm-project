﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject obj;
    Vector3 pos;
    void Start()
    {
        obj = GameObject.Find("Runner");
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        pos = new Vector3(obj.transform.position.x + 6.5f, obj.transform.position.y + 1.0f, obj.transform.position.z + 4.0f);
        this.gameObject.transform.position = pos;
        this.gameObject.transform.rotation = Quaternion.Euler(0, -90, 0);            //this.gameObject.transform.Translate(obj.GetComponent<PlayerMovement>().speed * Time.deltaTime, obj.transform.position.y, 0.0f);

    }
}
