﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBird : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Bird;
    public GameObject point;
    private GameObject TempBird;
    private float SpawnTime;
    private float speed;
    private float Yspeed;
    private int BirdDir;
    void Start()
    {
        BirdDir = 0;
        speed = 4.0f;
        Yspeed = 1.0f;
        TempBird = Instantiate(Bird, point.GetComponent<Transform>().position, Quaternion.Euler(0, 180, 0));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!TempBird)
            return;
        Movement();
    }

    void Movement()
    {
        TempBird.GetComponent<Transform>().Translate(0, 0, speed * Time.deltaTime);
        if (BirdDir == 0)
        {
            TempBird.GetComponent<Transform>().Translate(0, Yspeed * Time.deltaTime, 0);
            if (TempBird.GetComponent<Transform>().position.y >= 1.0)
                BirdDir = 1;
        }
        else if (BirdDir == 1)
        {
            TempBird.GetComponent<Transform>().Translate(0, -Yspeed * Time.deltaTime, 0);
            if (TempBird.GetComponent<Transform>().position.y <= -0.5)
                BirdDir = 0;
        }
    }
}
