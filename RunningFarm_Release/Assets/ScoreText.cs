﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    private int playerScore;
    public Text objText;
    private GameObject player;
    // Start is called before the first frame update
    private void Start()
    {
        player = GameObject.Find("Runner");
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        playerScore = player.GetComponent<PlayerMovement>().EatingNum;
        objText.text = ("  X  " + playerScore.ToString() );
    }
}
