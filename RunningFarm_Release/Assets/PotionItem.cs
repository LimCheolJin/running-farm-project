﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionItem : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource HiddenSound;
    private GameObject player;
    private void Start()
    {
        player = GameObject.Find("Runner");
        HiddenSound = GameObject.Find("HiddenItemSound").GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if (other.transform.tag == "Player")
        {
            if (player.GetComponent<PlayerMovement>().Hp < 100)
            {
                player.GetComponent<PlayerMovement>().Hp += 10;
                player.GetComponent<PlayerMovement>().hpbarSlider.value += 10;
            }
            else
            {
                player.GetComponent<PlayerMovement>().Hp = 100;
                player.GetComponent<PlayerMovement>().hpbarSlider.value = 100;
            }
            HiddenSound.Play();
            Destroy(this.gameObject);
        }
    }
}
