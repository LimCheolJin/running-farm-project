﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Load_manager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Road;
    public float time;
    private float Zpos = 40.0f;
    private int RoadCnt = 0;
   
    void Start()
    {
        StartCoroutine(Repeat());
    }
    private IEnumerator Repeat()
    {
        while(true)
        {
            if (RoadCnt< 20)
                time = 0.1f;
            else
                time = 3.0f;

           
            yield return new WaitForSeconds(time);

            Spawn();
        }
    }
    // Update is called once per dframe

    void Spawn()
    {
        Instantiate(Road, new Vector3(0, 0, Zpos), Quaternion.identity);
        Zpos += 20.0f;

        ++RoadCnt;
    }

}
