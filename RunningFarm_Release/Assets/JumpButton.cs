﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpButton : MonoBehaviour
{
    GameObject player;
    private void Start()
    {
        player = GameObject.Find("Runner");
    }
    public void Button()
    {
        player.GetComponent<PlayerMovement>()._isJumping = true;
    }

    public void ButtonUp()
    {
        player.GetComponent<PlayerMovement>()._isJumping = false;
    }
}
