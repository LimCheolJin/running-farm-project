﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideButton : MonoBehaviour
{
    private GameObject player;
    private void Start()
    {
        player = GameObject.Find("Runner");
    }
    public void ButtonDown()
    {
        player.GetComponent<PlayerMovement>().IsSliding = true;
    }

    public void ButtonUp()
    {
        player.GetComponent<PlayerMovement>().IsSliding = false;
    }


}
