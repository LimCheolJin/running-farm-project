﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HeightCompute : MonoBehaviour
{
    // Start is called before the first frame update
    private float playerHeight;
    int height;
    public Text heightText;
    private GameObject player;
    private void Start()
    {
        player = GameObject.Find("Runner");
    }
    void FixedUpdate()
    {

        playerHeight = player.transform.position.y;
        heightText.text = (" height " + playerHeight.ToString() );
    }
}
