﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternObstacleSpawnPoiint : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject point;
    public GameObject[] List;
    int num;
    private GameObject Obstacle;
    
    void Start()
    {
        num = Random.Range(0, 16);
        Instantiate(List[num], point.GetComponent<Transform>().position, Quaternion.identity);
    }

}
