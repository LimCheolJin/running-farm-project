﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorCollision : MonoBehaviour
{

    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if (other.transform.tag == "Player")
        { 
            Destroy(GameObject.Find("Floor(Clone)") );
            
        }
    }

}
