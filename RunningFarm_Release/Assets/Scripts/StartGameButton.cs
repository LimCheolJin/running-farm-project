﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameButton : MonoBehaviour
{
    public AudioSource sound;
    public void Button()
    {
        sound.Play();
        Invoke("startgame", 0.1f);
    }
    private void startgame()
    {
        SceneManager.LoadScene("SampleScene");
    }
   
}
