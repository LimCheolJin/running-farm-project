﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionObstacle : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject obj;
    public int damage;
    private Collider playercollider;
    private GameObject player;
    private AudioSource crySound;
    void Start()
    {
        crySound = GameObject.Find("Crying").GetComponent<AudioSource>();
        player = GameObject.Find("Runner");
        playercollider = GameObject.Find("Runner").GetComponent<CapsuleCollider>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other == playercollider && !player.GetComponent<PlayerMovement>().IsDash)
        {
           player.GetComponent<PlayerMovement>().Hp -= damage;
           player.GetComponent<PlayerMovement>().hpbarSlider.value -= damage;
           player.GetComponent<PlayerMovement>().speed = 0.0f;
           crySound.Play();
           if (player.GetComponent<PlayerMovement>().IsDead)
             crySound.Stop();
           player.GetComponent<PlayerMovement>().anim.SetBool("Run", false);
           player.GetComponent<PlayerMovement>().anim.Play("Crying", -1, 0.0f);

            Invoke("ReRun", 0.3f);
           
        }
    }
    void ReRun()
    {
        player.GetComponent<PlayerMovement>().anim.SetBool("Run", true);
        player.GetComponent<PlayerMovement>().speed = 5.0f;
    }
}
