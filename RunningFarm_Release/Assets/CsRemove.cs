﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CsRemove : MonoBehaviour
{
    void FixedUpdate()
    {
        Vector3 view = Camera.main.WorldToScreenPoint(transform.position);//월드 좌표를 스크린 좌표로 변형한다.
        if (view.x < -100)
        {
            Destroy(gameObject);    //스크린 좌표가 -20 이하일시 삭제   
        }
    }
}
