﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour
{
    public AudioSource sound;
    public void Button()
    {
        sound.Play();
        Invoke("Back", 0.2f);
    }

    private void Back()
    {
        SceneManager.LoadScene("GameStart");
    }
}
