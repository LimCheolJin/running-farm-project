﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWithPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource crysound;
    private GameObject player;
    void Start()
    {
        player = GameObject.Find("Runner");
        crysound = GameObject.Find("Crying").GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if (!player.GetComponent<PlayerMovement>().IsSliding)
        {
            if (player.GetComponent<CapsuleCollider>() == other && !player.GetComponent<PlayerMovement>().IsDash)
            {

                player.GetComponent<PlayerMovement>().Hp -= 10;
                player.GetComponent<PlayerMovement>().hpbarSlider.value -= 10;
                player.GetComponent<PlayerMovement>().speed = 0.0f;
                player.GetComponent<PlayerMovement>().anim.SetBool("Run", false);
                player.GetComponent<PlayerMovement>().anim.Play("Crying", -1, 0.0f);
                crysound.Play();

                Invoke("ReRun", 0.3f);

            }
        }
        else
        {
            if (player.GetComponent<BoxCollider>() == other && !player.GetComponent<PlayerMovement>().IsDash)
            {
                player.GetComponent<PlayerMovement>().Hp -= 10;
                player.GetComponent<PlayerMovement>().hpbarSlider.value -= 10;
                player.GetComponent<PlayerMovement>().speed = 0.0f;
                player.GetComponent<PlayerMovement>().anim.SetBool("Run", false);
                player.GetComponent<PlayerMovement>().anim.Play("Crying", -1, 0.0f);
                crysound.Play();

                Invoke("ReRun", 0.3f);
            }
        }
        if (player.GetComponent<CapsuleCollider>() == other && player.GetComponent<PlayerMovement>().IsDash )
        {
            
            this.gameObject.GetComponent<Animator>().Play("Death");
        }


    }

    void ReRun()
    {
        player.GetComponent<PlayerMovement>().anim.SetBool("Run", true);
        player.GetComponent<PlayerMovement>().speed = 5.0f;
    }
}
