﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 0.0f; // 이동속도
    public Animator anim; // 애니메이터
    [SerializeField]
    public Rigidbody rigidBody;
    public bool IsStart = false;
    public int Hp;
    public bool _isJumping = false;
    private float _posY;        //오브젝트의 초기 높이
    public float _jumpPower;   //점프력
    public float _jumpTime;    //점프 이후 경과시간

    [SerializeField]
    private float jumpForce;
    CapsuleCollider capsuleCollider;
    public int EatingNum;
    public bool IsDash = false;
    private Renderer render;
    public bool IsMargnet;
    private float MargnetTime;
    private float DashTime;
    public bool IsDead = false;
    public bool IsSliding = false;
    Color color;

    //
    BoxCollider box;
    CapsuleCollider capsule;

    //sound
    private AudioSource deathSound;
    private AudioSource powerUpSound;
    private bool powerUpSoundOnOff = true;

    //UI
    public Slider hpbarSlider;
    void Start()
    {
        deathSound = GameObject.Find("Death").GetComponent<AudioSource>();
        powerUpSound = GameObject.Find("PowerUp").GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
        anim.Play("Idle");
        _posY = transform.position.y;
        EatingNum = 0;
        speed = 5.0f;
        DashTime = 0.0f;
        Hp = 100;
        IsMargnet = false;
        IsDash = false;
        IsDead = false;
        MargnetTime = 0.0f;
        color = new Vector4(0.1f, 0.1f, 0.1f, 1.0f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
        if (!IsStart)
            anim.Play("Idle");
        if (Input.anyKeyDown)
            IsStart = true;
        if (IsStart && !IsDead)
        {
            Move();
            Jump();
            Dash();
            Sliding();
            Margnet();
            DeadCheck();
        }
        if (IsDead)
        {
            deathSound.Play();
            anim.Play("Die");
           // IsStart = false;

       
        }
    }
    private void DeadCheck()
    {
        if (this.gameObject.transform.position.y > 10.0f)
            Hp = 0;
        if (Hp <= 0)
        {
            PlayerPrefs.SetInt("EatingNum", EatingNum);
            IsDead = true;
            speed = 0.0f;
        }
    }
    private void Margnet()
    {
        if (!IsMargnet)
            return;
        if (IsMargnet)
        {
            MargnetTime += Time.deltaTime;
            
        }

        if (MargnetTime > 5.0f)
        {
            MargnetTime = 0.0f;
            IsMargnet = false;
        }
    }
    private void Sliding()
    {
        
        if (IsSliding)
        {
            anim.SetBool("Run", false);
            //anim.SetBool("Jump Without Root Motion", false);
            anim.SetBool("Dash", false);
            anim.Play("On the Ground Loop");
        }
        else if(!IsDash)
        {
            anim.SetBool("Run", true);
        }
    }
    private void Dash()
    {
        if (!IsDash)
            return;
        if(IsDash && IsStart)
        {
            if(powerUpSoundOnOff)
                powerUpSound.Play();
            anim.SetBool("Run", false);
            anim.SetBool("Dash", true);
            this.gameObject.GetComponent<Transform>().localScale = new Vector3(2.0f, 2.0f, 2.0f);
            speed = 10.0f;
            DashTime += Time.deltaTime;
            powerUpSoundOnOff = false;
        }
        if (DashTime > 5.0f)
        {
            powerUpSoundOnOff = true;
            IsDash = false;
            DashTime = 0.0f;
            anim.SetBool("Dash", false);
            anim.SetBool("Run", true);
            anim.Play("Run");
            speed = 5.0f;
            this.gameObject.GetComponent<Transform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }

    }
    private void Move()
    {
        transform.Translate(0, 0, speed * Time.deltaTime);
    }

    void Jump()
    {
        if (IsDash)
            return;

        if (_isJumping && IsStart && !IsSliding)
        {
            anim.SetBool("Run", false);
            anim.Play("Jump Without Root Motion");
            //_isJumping = true;
            _posY = transform.position.y;
        } //땅에닿을떄까지 run애니메이션은하지않기
        if (_isJumping)
        {
            rigidBody.AddForce(Vector3.up * _jumpPower, ForceMode.VelocityChange);
           // anim.SetBool("Run", true);
            _isJumping = false;
        }

    }
}
