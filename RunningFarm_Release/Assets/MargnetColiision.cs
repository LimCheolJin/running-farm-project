﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MargnetColiision : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource HiddenSound;
    private GameObject player;
    void Start()
    {
        player = GameObject.Find("Runner");
        HiddenSound = GameObject.Find("HiddenItemSound").GetComponent<AudioSource>();
    }


    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if(player.GetComponent<CapsuleCollider>() == other)
        {
            HiddenSound.Play();
            player.GetComponent<PlayerMovement>().IsMargnet = true;
            Destroy(this.gameObject);
        }
    }
}
