﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScore : MonoBehaviour
{
    // Start is called before the first frame update

    private int playerScore;
    public Text objText;
    private void Start()
    {
        playerScore = PlayerPrefs.GetInt("EatingNum");
    }
    // Update is called once per frame
    void FixedUpdate()
    {
       
        objText.text = " " + playerScore.ToString() + " ";
    }
}
