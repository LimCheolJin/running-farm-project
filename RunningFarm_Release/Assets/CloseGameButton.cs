﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseGameButton : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource sound;
    public void Button()
    {
        sound.Play();
        Invoke("Endgame", 0.1f);
    }

    private void Endgame()
    {
        //panel.GetComponent<FadeScript>().OnFade = true;
        Application.Quit();
    }
}
