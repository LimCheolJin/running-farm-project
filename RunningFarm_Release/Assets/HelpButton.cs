﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HelpButton : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource sound;
    public void Button()
    {
        sound.Play();
        Invoke("GotoHelpScene", 0.2f);
    }
    private void GotoHelpScene()
    {
        SceneManager.LoadScene("Help");
    }

    
}
