﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnItem : MonoBehaviour
{
    public GameObject[] Fruits;
    public GameObject point;
    private GameObject TempFruit;
    private float Yspeed;
    private int num;
    private int Dir;
    void Start()
    {
        num = Random.Range(0,4);
        Dir = Random.Range(0, 2);
        Yspeed = 2.0f;

        TempFruit=Instantiate(Fruits[num], point.GetComponent<Transform>().position, Quaternion.identity);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!TempFruit)
            return;
        Movement();
    }

    void Movement()
    {
        if (Dir == 0)
        {
            TempFruit.GetComponent<Transform>().Translate(0, Yspeed * Time.deltaTime, 0);
            if (TempFruit.GetComponent<Transform>().position.y >= 2.0)
                Dir = 1;
        }
        else if (Dir == 1)
        {
            TempFruit.GetComponent<Transform>().Translate(0, -Yspeed * Time.deltaTime, 0);
            if (TempFruit.GetComponent<Transform>().position.y <= 0.1)
                Dir = 0;
        }
    }

}
