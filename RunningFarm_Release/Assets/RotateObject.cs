﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject obj;
    public float RotateSpeed;


    // Update is called once per frame
    void Update()
    {
        obj.transform.Rotate(0,RotateSpeed * Time.deltaTime ,0);
    }
}
