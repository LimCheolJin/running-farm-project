﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSliding : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject obj;
    public int damage;
    private Collider playerboxcollider;
    private Collider playercapsulecollider;
    private GameObject player;
    private AudioSource crysound;
    void Start()
    {
        crysound = GameObject.Find("Crying").GetComponent<AudioSource>();
        player = GameObject.Find("Runner");
        playerboxcollider = GameObject.Find("Runner").GetComponent<BoxCollider>();
        playercapsulecollider = GameObject.Find("Runner").GetComponent<CapsuleCollider>();
    }
    private void OnTriggerEnter(Collider other)
    { 
        if(player.GetComponent<PlayerMovement>().IsSliding && !player.GetComponent<PlayerMovement>().IsDash)
        {
            
            if(other == playerboxcollider)
            {
                Debug.Log(player.GetComponent<PlayerMovement>().Hp);
                player.GetComponent<PlayerMovement>().Hp -= damage;
                player.GetComponent<PlayerMovement>().hpbarSlider.value -= damage;
                crysound.Play();
                if (player.GetComponent<PlayerMovement>().IsDead)
                    crysound.Stop();
                player.GetComponent<PlayerMovement>().speed = 0.0f;
                player.GetComponent<PlayerMovement>().anim.SetBool("Run", false);
                player.GetComponent<PlayerMovement>().anim.Play("Crying", -1, 0.0f);
                Invoke("ReRun", 0.3f);
            }
        }
        else if ( !player.GetComponent<PlayerMovement>().IsSliding && !player.GetComponent<PlayerMovement>().IsDash)
        {
            if (other == playercapsulecollider)
            {
                //Debug.Log(player.GetComponent<PlayerMovement>().Hp);
                player.GetComponent<PlayerMovement>().Hp -= damage;
                player.GetComponent<PlayerMovement>().hpbarSlider.value -= damage;
                crysound.Play();
                if (player.GetComponent<PlayerMovement>().IsDead)
                    crysound.Stop();
                player.GetComponent<PlayerMovement>().speed = 0.0f;
                player.GetComponent<PlayerMovement>().anim.SetBool("Run", false);
                player.GetComponent<PlayerMovement>().anim.Play("Crying", -1, 0.0f);
                Invoke("ReRun", 0.3f);
            }
        }
    }

    void ReRun()
    {
        player.GetComponent<PlayerMovement>().anim.SetBool("Run", true);
        player.GetComponent<PlayerMovement>().speed = 5.0f;
    }
}

