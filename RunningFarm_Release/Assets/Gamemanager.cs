﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gamemanager : MonoBehaviour
{
    GameObject player;
    private void Start()
    {
        player = GameObject.Find("Runner");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player.GetComponent<PlayerMovement>().IsDead)
        {
            Invoke("GotoGameOverScene", 1.5f);
        }   
    }

    void GotoGameOverScene()
    {
        SceneManager.LoadScene("GameOverScene");
        player.GetComponent<PlayerMovement>().IsDead = false;
    }
}
