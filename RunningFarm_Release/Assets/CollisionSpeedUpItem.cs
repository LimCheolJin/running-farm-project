﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSpeedUpItem : MonoBehaviour
{
    PlayerMovement obj;
    private AudioSource HiddenSound;
    private GameObject player;
    private void Start()
    {
        HiddenSound = GameObject.Find("HiddenItemSound").GetComponent<AudioSource>();
        obj = GameObject.Find("Runner").GetComponent<PlayerMovement>();
        player = GameObject.Find("Runner");
    }
    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if (player.GetComponent<CapsuleCollider>() == other)
        {
            Destroy(this.gameObject);
            HiddenSound.Play();
            obj.IsDash = true;
            obj.IsSliding = false;

        }
    }
}
